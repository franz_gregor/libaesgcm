# libaesgcm #

This repository is a fork of Intel's crypto library at [https://github.com/01org/isa-l_crypto.git](https://github.com/01org/isa-l_crypto.git) with the aim of only supporting AES-GCM. It was started for three reasons:

* to reduce the size of statically build binaries,
* to reduce build time, and
* to make the code run inside of SGX by omitting the CPUID instruction (necessary to decided whether the CPU supports SSE, AVX or AVX2).

## Usage ##

Dependencies:

* yasm (sudo apt install yasm)

Building:

```
#!bash

cd libaesgcm
make
cd ..
gcc -Ilibaesgcm/include libaesgcm/libaesgcm.a [...]

```


Have a look at include/aes_gcm.h for the available functions. Use the *{NAME}_avx_gen4()* variants to directly use the AVX2 implementation omitting the CPUID call and allow the linker to remove the AVX and SSE code from your binary. The key expansion functions have no AVX2 but only a AVX implementation. Thus, use the *aes_keyexp_{128|256}_avx()* variant.

## Note ##

This library only supports 16-byte tags whose last 4 bytes are 0x00000001!
