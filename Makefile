
YASM_CMD = yasm -f elf64

OBJ_FILES = $(addprefix obj/, gcm128_sse.o gcm128_avx_gen2.o gcm128_avx_gen4.o gcm256_sse.o gcm256_avx_gen2.o gcm256_avx_gen4.o gcm_multibinary.o keyexp_128.o keyexp_256.o keyexp_multibinary.o gcm_pre.o)

all: libaesgcm.a

obj:
	mkdir obj || true

obj/%.o: asm/%.asm obj
	yasm -f elf64 -o $@ $<

obj/%.o: src/%.c obj
	# -ffunction-sections -fdata-sections allows the linker to remove unused symbols
	# in our case this might be all 256bit routines or sse and avx implementation if the avx2 only funtions are used
	# TODO: this is not optimal yet: it either removes all symbols of a asm object file or none
	# this is a result of the assembly files containing one section for all symbols
	# thus the linker can not split them apart
	# You have to use "-Wl,--gc-sections" during linking to remove unused sections.
	$(CC) -ffunction-sections -fdata-sections -I./include -O2 -c -o $@ $<

libaesgcm.a: $(OBJ_FILES)
	ar cr $@ $^

clean:
	rm -rf libaesgcm.a obj/

.PHONY: clean all
